// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::key::CKey;
use crate::key::DATA_U64_SIZE;
use std::num::Wrapping;

impl std::ops::Add<CKey> for CKey {
    type Output = Self;

    /// Add another key.
    ///
    /// If the result is outside key space, will loop back,
    /// as in wrapping mode. So the result is always within key space.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k1 = CKey::from(0.6f64);
    /// let k2 = CKey::from(0.8f64);
    /// let k_sum = k1 + k2;
    /// assert_eq!("0.400000000", String::from(k_sum));
    /// ```
    fn add(self, other: CKey) -> CKey {
        let mut old_carry = 0;
        let mut ret: CKey = CKey::default();
        for i in 0..DATA_U64_SIZE {
            let j = DATA_U64_SIZE - i - 1;
            let (v, carry) = match self.data[j].checked_add(other.data[j]) {
                Some(v1) => match v1.checked_add(old_carry) {
                    Some(v2) => (v2, 0),
                    None => ((Wrapping(v1) + Wrapping(old_carry)).0, 1),
                },
                None => (
                    (Wrapping(self.data[j]) + Wrapping(other.data[j]) + Wrapping(old_carry)).0,
                    1,
                ),
            };
            ret.data[j] = v;
            old_carry = carry;
        }
        ret
    }
}

impl std::ops::Sub<CKey> for CKey {
    type Output = Self;

    /// Sub another key.
    ///
    /// If the result is outside key space, will loop back,
    /// as in wrapping mode. So the result is always within key space.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k1 = CKey::from(0.5f64);
    /// let k2 = CKey::from(0.9f64);
    /// let k_diff = k1 - k2;
    /// assert_eq!("0.600000000", String::from(k_diff));
    /// ```
    fn sub(self, other: CKey) -> CKey {
        let mut old_carry = 0;
        let mut ret: CKey = CKey::default();
        for i in 0..DATA_U64_SIZE {
            let j = DATA_U64_SIZE - i - 1;
            let (v, carry) = match self.data[j].checked_sub(other.data[j]) {
                Some(v1) => match v1.checked_sub(old_carry) {
                    Some(v2) => (v2, 0),
                    None => ((Wrapping(v1) - Wrapping(old_carry)).0, 1),
                },
                None => (
                    (Wrapping(self.data[j]) - Wrapping(other.data[j]) - Wrapping(old_carry)).0,
                    1,
                ),
            };
            ret.data[j] = v;
            old_carry = carry;
        }
        ret
    }
}

impl std::ops::Add<f32> for CKey {
    type Output = Self;

    /// Add an f32.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.3f32);
    /// // due to rounding errors, not exactly 0.7
    /// assert_eq!("0.700000018", String::from(k + 0.4f32));
    /// ```
    fn add(self, delta: f32) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<f32> for CKey {
    type Output = Self;

    /// Sub an f32.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.3f32);
    /// // due to rounding errors, not exactly 0.9
    /// assert_eq!("0.900000006", String::from(k - 0.4f32));
    /// ```
    fn sub(self, delta: f32) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<f64> for CKey {
    type Output = Self;

    /// Add an f64.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.3f64);
    /// assert_eq!("0.700000000", String::from(k + 0.4f64));
    /// ```
    fn add(self, delta: f64) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<f64> for CKey {
    type Output = Self;

    /// Sub an f64.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.3f64);
    /// assert_eq!("0.900000000", String::from(k - 0.4f64));
    /// ```
    fn sub(self, delta: f64) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<u8> for CKey {
    type Output = Self;

    /// Add a u8.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^8.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.490625000", String::from(k + 100u8))
    /// ```
    fn add(self, delta: u8) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<u8> for CKey {
    type Output = Self;

    /// Sub a u8.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^8.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.709375000", String::from(k - 100u8))
    /// ```
    fn sub(self, delta: u8) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<i8> for CKey {
    type Output = Self;

    /// Add a i8.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^8.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.490625000", String::from(k + 100i8))
    /// ```
    fn add(self, delta: i8) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<i8> for CKey {
    type Output = Self;

    /// Sub a i8.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^8.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.709375000", String::from(k - 100i8))
    /// ```
    fn sub(self, delta: i8) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<u16> for CKey {
    type Output = Self;

    /// Add a u16.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.252587891", String::from(k + 10_000u16))
    /// ```
    fn add(self, delta: u16) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<u16> for CKey {
    type Output = Self;

    /// Sub a u16.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.947412109", String::from(k - 10_000u16))
    /// ```
    fn sub(self, delta: u16) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<i16> for CKey {
    type Output = Self;

    /// Add a i16.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.252587891", String::from(k + 10_000i16))
    /// ```
    fn add(self, delta: i16) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<i16> for CKey {
    type Output = Self;

    /// Sub a i16.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.947412109", String::from(k - 10_000i16))
    /// ```
    fn sub(self, delta: i16) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<u32> for CKey {
    type Output = Self;

    /// Add a u32.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^32.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.100232831", String::from(k + 1_000_000u32))
    /// ```
    fn add(self, delta: u32) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<u32> for CKey {
    type Output = Self;

    /// Sub a u32.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^32.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.099767169", String::from(k - 1_000_000u32))
    /// ```
    fn sub(self, delta: u32) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<i32> for CKey {
    type Output = Self;

    /// Add a i32.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^32.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.100232831", String::from(k + 1_000_000i32))
    /// ```
    fn add(self, delta: i32) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<i32> for CKey {
    type Output = Self;

    /// Sub a i32.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^32.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.099767169", String::from(k - 1_000_000i32))
    /// ```
    fn sub(self, delta: i32) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<u64> for CKey {
    type Output = Self;

    /// Add a u64.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^64.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.100542101", String::from(k + 10_000_000_000_000_000u64))
    /// ```
    fn add(self, delta: u64) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<u64> for CKey {
    type Output = Self;

    /// Sub a u64.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^64.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.099457899", String::from(k - 10_000_000_000_000_000u64))
    /// ```
    fn sub(self, delta: u64) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

impl std::ops::Add<i64> for CKey {
    type Output = Self;

    /// Add a i64.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^64.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.100542101", String::from(k + 10_000_000_000_000_000i64))
    /// ```
    fn add(self, delta: i64) -> CKey {
        let other = CKey::from(delta);
        self + other
    }
}

impl std::ops::Sub<i64> for CKey {
    type Output = Self;

    /// Sub a i64.
    ///
    /// The value is first converted to a key, considering key space
    /// goes from 0 to 2^64.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.1f64);
    /// assert_eq!("0.099457899", String::from(k - 10_000_000_000_000_000i64))
    /// ```
    fn sub(self, delta: i64) -> CKey {
        let other = CKey::from(delta);
        self - other
    }
}

#[cfg(test)]
mod tests {
    use super::CKey;

    #[test]
    fn test_ckey_add() {
        assert_eq!(
            "0.300000000",
            format!("{}", CKey::from(0.1f64) + CKey::from(0.2f64))
        );
        assert_eq!(
            "0.000000000",
            format!("{}", CKey::from(0.5f64) + CKey::from(0.5f64))
        );
        assert_eq!(
            "0.100000000",
            format!("{}", CKey::from(0.8f64) + CKey::from(0.3f64))
        );
        assert_eq!(
            "0.300000000",
            format!("{}", CKey::from(1.1f64) + CKey::from(0.2f64))
        );
        assert_eq!(
            "0.400000000",
            format!("{}", CKey::from(-0.1f64) + CKey::from(0.5f64))
        );
    }

    #[test]
    fn test_ckey_sub() {
        assert_eq!(
            "0.300000000",
            format!("{}", CKey::from(0.4f64) - CKey::from(0.1f64))
        );
        assert_eq!(
            "0.000000000",
            format!("{}", CKey::from(0.9f64) - CKey::from(0.9f64))
        );
        assert_eq!(
            "0.900000000",
            format!("{}", CKey::from(0.2f64) - CKey::from(0.3f64))
        );
        assert_eq!(
            "0.800000000",
            format!("{}", CKey::from(1.1f64) - CKey::from(1.3f64))
        );
        assert_eq!(
            "0.900000000",
            format!("{}", CKey::from(0.2f64) - CKey::from(-0.7f64))
        );
    }

    #[test]
    fn test_ckey_add_f32() {
        assert_eq!("0.300000004", format!("{}", CKey::from(0.1f32) + 0.2f32));
        assert_eq!("0.000000000", format!("{}", CKey::from(0.5f32) + 0.5f32));
        assert_eq!("0.100000024", format!("{}", CKey::from(0.8f32) + 0.3f32));
        assert_eq!("0.300000027", format!("{}", CKey::from(1.1f32) + 0.2f32));
        assert_eq!("0.399999976", format!("{}", CKey::from(-0.1f32) + 0.5f32));
    }

    #[test]
    fn test_ckey_sub_f32() {
        assert_eq!("0.300000004", format!("{}", CKey::from(0.4f32) - 0.1f32));
        assert_eq!("0.000000000", format!("{}", CKey::from(0.9f32) - 0.9f32));
        assert_eq!("0.899999991", format!("{}", CKey::from(0.2f32) - 0.3f32));
        assert_eq!("0.800000072", format!("{}", CKey::from(1.1f32) - 1.3f32));
        assert_eq!("0.899999991", format!("{}", CKey::from(0.2f32) - -0.7f32));
    }

    #[test]
    fn test_ckey_add_f64() {
        assert_eq!("0.300000000", format!("{}", CKey::from(0.1f64) + 0.2f64));
        assert_eq!("0.000000000", format!("{}", CKey::from(0.5f64) + 0.5f64));
        assert_eq!("0.100000000", format!("{}", CKey::from(0.8f64) + 0.3f64));
        assert_eq!("0.300000000", format!("{}", CKey::from(1.1f64) + 0.2f64));
        assert_eq!("0.400000000", format!("{}", CKey::from(-0.1f64) + 0.5f64));
    }

    #[test]
    fn test_ckey_sub_f64() {
        assert_eq!("0.300000000", format!("{}", CKey::from(0.4f64) - 0.1f64));
        assert_eq!("0.000000000", format!("{}", CKey::from(0.9f64) - 0.9f64));
        assert_eq!("0.900000000", format!("{}", CKey::from(0.2f64) - 0.3f64));
        assert_eq!("0.800000000", format!("{}", CKey::from(1.1f64) - 1.3f64));
        assert_eq!("0.900000000", format!("{}", CKey::from(0.2f64) - -0.7f64));
    }

    #[test]
    fn test_ckey_add_sub_ui8() {
        let mut k = CKey::zero();

        // u8/i8
        k = k + 10u8;
        assert_eq!("0.039062500", format!("{}", k));
        k = k - 20u8;
        assert_eq!("0.960937500", format!("{}", k));
        k = k + (-5i8);
        assert_eq!("0.941406250", format!("{}", k));
        k = k - (-15i8);
        assert_eq!(CKey::zero(), k);

        // u16/i16
        k = k + 10_000u16;
        assert_eq!("0.152587891", format!("{}", k));
        k = k - 20_000u16;
        assert_eq!("0.847412109", format!("{}", k));
        k = k + (-5_000i16);
        assert_eq!("0.771118164", format!("{}", k));
        k = k - (-15_000i16);
        assert_eq!(CKey::zero(), k);

        // u32/i32
        k = k + 100_000_000u32;
        assert_eq!("0.023283064", format!("{}", k));
        k = k - 200_000_000u32;
        assert_eq!("0.976716936", format!("{}", k));
        k = k + (-50_000_000i32);
        assert_eq!("0.965075403", format!("{}", k));
        k = k - (-150_000_000i32);
        assert_eq!(CKey::zero(), k);

        // u64/i64
        k = k + 1_000_000_000_000_000_000u64;
        assert_eq!("0.054210109", format!("{}", k));
        k = k - 2_000_000_000_000_000_000u64;
        assert_eq!("0.945789891", format!("{}", k));
        k = k + (-500_000_000_000_000_000i64);
        assert_eq!("0.918684837", format!("{}", k));
        k = k - (-1_500_000_000_000_000_000i64);
        assert_eq!(CKey::zero(), k);
    }
}
