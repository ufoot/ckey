use crate::key::CKey;

const ONE_I128: i128 = 0x1_0000_0000_0000_0000;
const ONE_F64: f64 = ONE_I128 as f64;
const ONE_F32: f32 = ONE_I128 as f32;

impl From<f32> for CKey {
    /// Convert from an f32, considering the ring goes from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.5f32);
    /// assert_eq!("0.500000000", format!("{}", k));
    /// ```
    fn from(f: f32) -> CKey {
        let f_mod_1: f32 = if f < 0.0 {
            1.0 - ((-f).fract())
        } else {
            f.fract()
        };
        let v = (ONE_F32 * f_mod_1) as u64;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<f64> for CKey {
    /// Convert from an f64, considering the ring goes from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.5f64);
    /// assert_eq!("0.500000000", format!("{}", k));
    /// ```
    fn from(f: f64) -> CKey {
        let f_mod_1: f64 = if f < 0.0 {
            1.0 - ((-f).fract())
        } else {
            f.fract()
        };
        let v = (ONE_F64 * f_mod_1) as u64;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<u8> for CKey {
    /// Convert from a u8, considering the ring goes from 0 to 2^8.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(64u8);
    /// assert_eq!("0.250000000", format!("{}", k));
    /// ```
    fn from(u: u8) -> CKey {
        let v = (u as u64) << 56;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<i8> for CKey {
    /// Convert from a i8, considering the ring goes from 0 to 2^8.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(-64i8);
    /// assert_eq!("0.750000000", format!("{}", k));
    /// ```
    fn from(i: i8) -> CKey {
        let v = (i as u64) << 56;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<u16> for CKey {
    /// Convert from a u16, considering the ring goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(16384u16);
    /// assert_eq!("0.250000000", format!("{}", k));
    /// ```
    fn from(u: u16) -> CKey {
        let v = (u as u64) << 48;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<i16> for CKey {
    /// Convert from a i16, considering the ring goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(-16384i16);
    /// assert_eq!("0.750000000", format!("{}", k));
    /// ```
    fn from(i: i16) -> CKey {
        let v = (i as u64) << 48;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<u32> for CKey {
    /// Convert from a u32, considering the ring goes from 0 to 2^32.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(1073741824u32);
    /// assert_eq!("0.250000000", format!("{}", k));
    /// ```
    fn from(u: u32) -> CKey {
        let v = (u as u64) << 32;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<i32> for CKey {
    /// Convert from a i32, considering the ring goes from 0 to 2^32.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(-1073741824i32);
    /// assert_eq!("0.750000000", format!("{}", k));
    /// ```
    fn from(i: i32) -> CKey {
        let v = (i as u64) << 32;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<u64> for CKey {
    /// Convert from a u64, considering the ring goes from 0 to 2^64.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(1000000000000000000u64);
    /// assert_eq!("0.054210109", format!("{}", k));
    /// ```
    fn from(u: u64) -> CKey {
        CKey { data: [u, 0, 0, 0] }
    }
}

impl From<i64> for CKey {
    /// Convert from a i64, considering the ring goes from 0 to 2^64.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(-1000000000000000000i64);
    /// assert_eq!("0.945789891", format!("{}", k));
    /// ```
    fn from(i: i64) -> CKey {
        let v = i as u64;
        CKey { data: [v, 0, 0, 0] }
    }
}

impl From<CKey> for f32 {
    /// Convert to an f32, considering the ring goes from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("123456ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!("0.071111143", format!("{:0.9}", f32::from(k)));
    /// ```
    fn from(k: CKey) -> f32 {
        (k.data[0] as f32) / ONE_F32
    }
}

impl From<CKey> for f64 {
    /// Convert to an f64, considering the ring goes from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("123456ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!("0.071111143", format!("{:0.9}", f64::from(k)));
    /// ```
    fn from(k: CKey) -> f64 {
        (k.data[0] as f64) / ONE_F64
    }
}

impl From<CKey> for u8 {
    /// Convert to a u8, considering the ring goes from 0 to 2^8.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("123456ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!(18, u8::from(k));
    /// ```
    fn from(k: CKey) -> u8 {
        (k.data[0] >> 56) as u8
    }
}

impl From<CKey> for i8 {
    /// Convert to a i8, considering the ring goes from 0 to 2^8.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("987654ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!(-104, i8::from(k));
    /// ```
    fn from(k: CKey) -> i8 {
        (k.data[0] >> 56) as i8
    }
}

impl From<CKey> for u16 {
    /// Convert to a u16, considering the ring goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("123456ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!(4660, u16::from(k));
    /// ```
    fn from(k: CKey) -> u16 {
        (k.data[0] >> 48) as u16
    }
}

impl From<CKey> for i16 {
    /// Convert to a i16, considering the ring goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("987654ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!(-26506, i16::from(k));
    /// ```
    fn from(k: CKey) -> i16 {
        (k.data[0] >> 48) as i16
    }
}

impl From<CKey> for u32 {
    /// Convert to a u32, considering the ring goes from 0 to 2^16.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("123456ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!(305420031, u32::from(k));
    /// ```
    fn from(k: CKey) -> u32 {
        (k.data[0] >> 32) as u32
    }
}

impl From<CKey> for i32 {
    /// Convert to a i32, considering the ring goes from 0 to 2^32.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("987654ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!(-1737075457, i32::from(k));
    /// ```
    fn from(k: CKey) -> i32 {
        (k.data[0] >> 32) as i32
    }
}

impl From<CKey> for u64 {
    /// Convert to a u64, considering the ring goes from 0 to 2^64.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("123456ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!(1311769048983273471, u64::from(k));
    /// ```
    fn from(k: CKey) -> u64 {
        k.data[0]
    }
}

impl From<CKey> for i64 {
    /// Convert to a i64, considering the ring goes from 0 to 2^64.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::parse("987654ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").unwrap();
    /// assert_eq!(-7460682274204286977, i64::from(k));
    /// ```
    fn from(k: CKey) -> i64 {
        k.data[0] as i64
    }
}

impl std::convert::TryFrom<&str> for CKey {
    type Error = hex::FromHexError;

    /// Try to convert from a &str.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::try_from("12345678ffffffff01234567fffffffff00123456ffffffff0012345ffffffff").unwrap();
    /// assert_eq!("12345678ffffffff01234567fffffffff00123456ffffffff0012345ffffffff", format!("{:?}", k));
    /// ```
    fn try_from(value: &str) -> Result<CKey, Self::Error> {
        Self::parse(value)
    }
}

impl std::convert::TryFrom<String> for CKey {
    type Error = hex::FromHexError;

    /// Try to convert from a String.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::try_from(String::from("12345678ffffffff01234567fffffffff00123456ffffffff0012345ffffffff")).unwrap();
    /// assert_eq!("12345678ffffffff01234567fffffffff00123456ffffffff0012345ffffffff", format!("{:?}", k));
    /// ```
    fn try_from(value: String) -> Result<CKey, Self::Error> {
        Self::try_from(value.as_str())
    }
}

impl From<CKey> for String {
    /// Convert to a String.
    ///
    /// The representation it gives is the fmt::Display impl,
    /// which gives a short (not complete/unique) yet readable
    /// representation of the key, as a floating point from 0 to 1.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(0.6 as f64);
    /// assert_eq!("0.600000000", String::from(k));
    /// ```
    fn from(k: CKey) -> String {
        format!("{}", k)
    }
}

#[cfg(feature = "serde")]
impl From<Vec<u8>> for CKey {
    /// Convert from a vector of bytes.
    ///
    /// The vector can be any length, extra bytes are ignored,
    /// missing bytes are replaced with zeros.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(vec![1, 2, 3]);
    /// assert_eq!("0102030000000000000000000000000000000000000000000000000000000000", format!("{:?}", k));
    /// ```
    fn from(v: Vec<u8>) -> CKey {
        CKey::set(&v)
    }
}

#[cfg(feature = "serde")]
impl From<CKey> for Vec<u8> {
    /// Convert to a vector.
    ///
    /// Gives a big-endian representation of the key. 32 bytes.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(10u8);
    /// let vec: Vec<u8> = Vec::from(k);
    /// assert_eq!(32, vec.len());
    /// assert_eq!(10, vec[0]);
    /// ```
    fn from(k: CKey) -> Vec<u8> {
        k.bytes()
    }
}

#[cfg(feature = "serde")]
impl From<&Vec<u8>> for CKey {
    /// Convert from a vector of bytes.
    ///
    /// The vector can be any length, extra bytes are ignored,
    /// missing bytes are replaced with zeros.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(&vec![4, 5, 6, 7]);
    /// assert_eq!("0405060700000000000000000000000000000000000000000000000000000000", format!("{:?}", k));
    /// ```
    fn from(v: &Vec<u8>) -> CKey {
        CKey::set(v)
    }
}

#[cfg(feature = "serde")]
impl From<&CKey> for Vec<u8> {
    /// Convert to a vector.
    ///
    /// Gives a big-endian representation of the key. 32 bytes.
    ///
    /// # Examples
    /// ```
    /// use ckey::CKey;
    ///
    /// let k = CKey::from(10u8);
    /// let vec: Vec<u8> = Vec::from(&k);
    /// assert_eq!(32, vec.len());
    /// assert_eq!(10, vec[0]);
    /// ```
    fn from(k: &CKey) -> Vec<u8> {
        k.bytes()
    }
}
