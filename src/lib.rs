// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! [CKey](https://gitlab.com/liberecofr/ckey) is an experimental consistent hash key library.
//!
//! ![CKey icon](https://gitlab.com/liberecofr/ckey/raw/main/media/ckey-256x256.jpg)
//!
//! Not ready for production use.

mod convert;
mod format;
mod key;
mod math;

pub use convert::*;
pub use format::*;
pub use key::*;
pub use math::*;
