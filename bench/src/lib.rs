#![feature(test)]
extern crate ckey;
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use ckey::CKey;
    use test::Bencher;

    #[bench]
    fn bench_from_f64(b: &mut Bencher) {
        let mut key = CKey::zero();
        let mut f = 0.0;
        b.iter(|| {
            key = CKey::from(f);
            f += 0.07;
        });
        println!("f: {}", f);
    }

    #[bench]
    fn bench_incr(b: &mut Bencher) {
        let mut key = CKey::zero();
        b.iter(|| {
            key.incr();
        });
        println!("key: {}", key);
    }

    #[bench]
    fn bench_bytes(b: &mut Bencher) {
        let key = CKey::halfway();
        let mut vec: Vec<u8> = Vec::new();
        b.iter(|| {
            vec = key.bytes();
        });
        println!("vec: {:?}", vec);
    }

    #[bench]
    fn bench_inside(b: &mut Bencher) {
        let x = CKey::from(0.1);
        let y = CKey::from(0.5);
        let z = CKey::from(0.9);
        let mut inside = false;
        b.iter(|| {
            inside = x.inside(y, z);
        });
        println!("inside: {:?}", inside);
    }

    #[bench]
    fn bench_serial(b: &mut Bencher) {
        let mut s = CKey::zero();
        b.iter(|| {
            s = CKey::serial("some random content");
        });
        println!("s: {}", s);
        assert!(s != CKey::zero());
    }

    #[bench]
    fn bench_display(b: &mut Bencher) {
        let k = CKey::from(0.123456789);
        let mut f = String::new();
        b.iter(|| {
            f = format!("{}", k);
        });
        println!("f: {}", f);
        assert!("" != f);
    }
}
