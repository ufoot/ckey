# CKey

[CKey](https://gitlab.com/liberecofr/ckey) is an experimental consistent hash key library, implemented in [Rust](https://www.rust-lang.org/).

See the [theory about consistent hashing](https://en.wikipedia.org/wiki/Consistent_hashing).
This library could be used as a brick to implement a [Chord](https://en.wikipedia.org/wiki/Chord_(peer-to-peer)) ring.
It provides 256-bit keys, with some helpers to add, compare, and know their position on the ring.
Internally, keys are stored on 4 unsigned integers of 64-bit each.

![CKey icon](https://gitlab.com/liberecofr/ckey/raw/main/media/ckey-256x256.jpg)

# Status

For now this is a toy project, clearly *NOT* suitable for production use.

[![Build Status](https://gitlab.com/liberecofr/ckey/badges/main/pipeline.svg)](https://gitlab.com/liberecofr/ckey/pipelines)
[![Crates.io](https://img.shields.io/crates/v/ckey.svg)](https://crates.io/crates/ckey)
[![Gitlab](https://img.shields.io/gitlab/last-commit/liberecofr/ckey)](https://gitlab.com/liberecofr/ckey/tree/main)
[![License](https://img.shields.io/gitlab/license/liberecofr/ckey)](https://gitlab.com/liberecofr/ckey/blob/main/LICENSE)

# Usage

```rust
use ckey::CKey;

let k1 = CKey::rand();
let k2 = k1.next();
let k3 = k2 + 10u8;
assert!(k2.inside(k1, k3));
let k4 = CKey::from(1000u16);
assert_eq!("0.015258789", format!("{}", k4));
```

# Benchmarks

Taken from a random CI job:

```
running 6 tests
test tests::bench_bytes    ... bench:          28 ns/iter (+/- 0)
test tests::bench_display  ... bench:         204 ns/iter (+/- 3)
test tests::bench_from_f64 ... bench:          10 ns/iter (+/- 0)
test tests::bench_incr     ... bench:           2 ns/iter (+/- 0)
test tests::bench_inside   ... bench:          10 ns/iter (+/- 1)
test tests::bench_serial   ... bench:         468 ns/iter (+/- 106)
test result: ok. 0 passed; 0 failed; 0 ignored; 6 measured; 0 filtered out; finished in 6.37s
```

This is not the result of extensive, thorough benchmarking, just a random
snapshot at some point in development history.

TL;DR relatively fast, though slower than standard integer ops.

The implementation tries to optimize for the speed of operations like `incr` or `inside` which
are typically repeated a lot of times in a real-world program. OTOH creation and serialization
of keys are rather costly.

To run the benchmarks:

```shell
cd bench
rustup default nightly
cargo bench
```

# Links

* [crate](https://crates.io/crates/ckey) on crates.io
* [doc](https://docs.rs/ckey/) on docs.rs
* [source](https://gitlab.com/liberecofr/ckey/tree/main) on gitlab.com

# License

Ckey is licensed under the [MIT](https://gitlab.com/liberecofr/ckey/blob/main/LICENSE) license.
